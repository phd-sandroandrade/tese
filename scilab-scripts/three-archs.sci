// Sistema massa-mola
t = 0:(3/(maxtime-changetime)):3;
m = 1;  // Mass : kg (quanto maior, maior o settling time)
b = 5; // Damper : Ns/m (quanto maior, maior o tempo de subida)
k = 24; // Spring : N/m (quanto maior, maior o overshoot)
System = k/(m*%s^2 + b*%s + k);
y1 = oldref + (newref-oldref)*csim('step', t , System);

mytime = cat(2, [0'], 5*changetime+5*t*(maxtime-changetime)/3);
throughput = cat(2, [oldref'], y1);
plot(mytime,throughput, 'r');

t = 0:(3/(maxtime-changetime)):3;
m = 0.75;  // Mass : kg (quanto maior, maior o settling time)
b = 5; // Damper : Ns/m (quanto maior, maior o tempo de subida)
k = 44; // Spring : N/m (quanto maior, maior o overshoot)
System = k/(m*%s^2 + b*%s + k);
y1 = oldref + (newref-oldref)*csim('step', t , System);

mytime = cat(2, [0'], 5*changetime+5*t*(maxtime-changetime)/3);
throughput = cat(2, [oldref'], y1);
plot(mytime,throughput, 'g');

t = 0:(3/(maxtime-changetime)):3;
m = 1.5;  // Mass : kg (quanto maior, maior o settling time)
b = 5; // Damper : Ns/m (quanto maior, maior o tempo de subida)
k = 45; // Spring : N/m (quanto maior, maior o overshoot)
System = k/(m*%s^2 + b*%s + k);
y1 = oldref + (newref-oldref)*csim('step', t , System);

mytime = cat(2, [0'], 5*changetime+5*t*(maxtime-changetime)/3);
throughput = cat(2, [oldref'], y1);
plot(mytime,throughput, 'b');