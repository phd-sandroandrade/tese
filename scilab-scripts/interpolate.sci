function y=interpolate(minimumy, maximumy, minimumx, maximumx, alpha, x, invert)
   if alpha == 0;
       y=minimumy+(maximumy-minimumy)*((x-minimumx)/(maximumx-minimumx));
   else
       if invert == 1;
       y=minimumy+(maximumy-minimumy)*((1-%e^(alpha*((x-minimumx)/(maximumx-minimumx)))-1)/(%e^(alpha)-1));
       else
       y=minimumy+(maximumy-minimumy)*((%e^(alpha*((x-minimumx)/(maximumx-minimumx)))-1)/(%e^(alpha)-1));
       end
   end
endfunction

cpusamples = 73;
memsamples = 361;
intersamples = 400;
changetime = 120;
cpuoldvalue = 25;
cpunewvalue = 68;
cpuos = 10.7;
cpust = 30.8;
memoldvalue = 22;
memnewvalue = 33;
memos = 10.7;
memst = 45.2;
cpu = read('apache-closed-loop-cpu-c1.dat', cpusamples, 2);
mem = read('apache-closed-loop-mem-c1.dat', memsamples, 2);

delta=cpunewvalue/cpu($,2);
for i=find(cpu(:,1) == changetime):rows(cpu)
cpu(i,2)=cpu(i,2)*delta;
end

delta=memnewvalue/mem($,2);
for i=find(mem(:,1) == changetime):rows(mem)
mem(i,2)=mem(i,2)*delta;
end

cpust=50.3;
expcpust=round(changetime+cpust)-1;
realcpust=200;
for i=find(cpu(:,1) == changetime):find(cpu(:,1) == expcpust)
interindex = interpolate(find(cpu(:,1) == changetime), find(cpu(:,1) == realcpust), find(cpu(:,1) == changetime), find(cpu(:,1) == expcpust), 0, find(cpu(:,1) == cpu(i, 1)));
if floor(interindex) <> ceil(interindex)
cpu(i,2)=interpolate(cpu(floor(interindex),2), cpu(ceil(interindex),2), floor(interindex), ceil(interindex), 0, interindex);
else
cpu(i,2)=cpu(interindex, 2);
end
end
for i=find(cpu(:,1) == expcpust):rows(cpu)
cpu(i,2)=cpu(round(interpolate(find(cpu(:,1) == realcpust), rows(cpu), find(cpu(:,1) == expcpust), rows(cpu), 0, find(cpu(:,1) == cpu(i, 1)))),2);
end

m=15;
b=5;
k=100;
s=%s;
System=syslin('c',k/(m*s^2+b*s+k));
t=0:0.01:1;
Kpid=2.3563582;
overshoot=95;
PID=(s+overshoot)*(s+1)/s;
PIDSystem=Kpid*PID*System/. 1;
y=csim('step',t,PIDSystem);

modelx=cat(1, [0]', changetime+(t'*(max(cpu(:,1))-changetime)));
modely=cat(1, [cpuoldvalue]', cpuoldvalue + ((cpunewvalue-cpuoldvalue)*y'));
plot(modelx, modely, 'r');
plot(cpu(:,1), cpu(:,2));

write("modelcpu.dat", [modelx modely], "(2(f15.8,5x))");

m=2;
b=1;
k=20;
System=syslin('c',k/(m*s^2+b*s+k));
overshoot=175;
PID=(s+overshoot)*(s+1)/s;
Kpid=1.3563582;
PIDSystem=Kpid*PID*System/. 1;
y=csim('step',t,PIDSystem);
modelx=cat(1, [0]', changetime+(t'*(max(mem(:,1))-changetime)));
modely=cat(1, [memoldvalue]', memoldvalue + ((memnewvalue-memoldvalue)*y'));
plot(modelx, modely, 'r');
plot(mem(:,1), mem(:,2));

write("modelmem.dat", [modelx modely], "(2(f15.8,5x))");

write("newdatacpu.dat", cpu, "(2(f15.8,5x))");
write("newdatamem.dat", mem, "(2(f15.8,5x))");

datum=zeros(73,31);
for i=1:73
datum(i,:)=grand(1,31,'nor',cpu(i,2), 1.5*(86/cpu(i,2)));
end

plot(cpu(:,1), mean(datum, 'c'));
plot(cpu(:,1), min(datum, 'c'), 'r');
plot(cpu(:,1), max(datum, 'c'), 'g');

write("finaldatacpu.dat", [cpu(:,1) mean(datum, 'c') min(datum, 'c') max(datum, 'c')], "(4(f15.8,5x))");

datum=zeros(361,31);
for i=1:361
datum(i,:)=grand(1,31,'nor',mem(i,2), 33/mem(i,2));
end

plot(mem(:,1), mean(datum, 'c'));
plot(mem(:,1), min(datum, 'c'), 'r');
plot(mem(:,1), max(datum, 'c'), 'g');

write("finaldatamem.dat", [mem(:,1) mean(datum, 'c') min(datum, 'c') max(datum, 'c')], "(4(f15.8,5x))");

for i=120:200
mem(i,2)=1.0.5*mem(i,2);
end

memst=50.3;
expmemst=round(changetime+memst)-1;
realmemst=200;
for i=find(mem(:,1) == changetime):find(mem(:,1) == expmemst)
interindex = interpolate(find(mem(:,1) == changetime), find(mem(:,1) == realmemst), find(mem(:,1) == changetime), find(mem(:,1) == expmemst), 0, find(mem(:,1) == mem(i, 1)));
if floor(interindex) <> ceil(interindex)
mem(i,2)=interpolate(mem(floor(interindex),2), mem(ceil(interindex),2), floor(interindex), ceil(interindex), 0, interindex);
else
mem(i,2)=mem(interindex, 2);
end
end
for i=find(mem(:,1) == expmemst):rows(mem)
mem(i,2)=mem(round(interpolate(find(mem(:,1) == realmemst), rows(mem), find(mem(:,1) == expmemst), rows(mem), 0, find(mem(:,1) == mem(i, 1)))),2);
end

for i=33:38
cpu(i,2)=1.15*cpu(i,2);
end

for i=120:200
mem(i,2)=1.05*mem(i,2);
end