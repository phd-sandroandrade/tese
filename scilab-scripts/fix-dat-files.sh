#!/bin/bash

cat $1 | sed "s/1.00000000/1/g" | sed "s/2.00000000/2/g" | sed "s/3.00000000/3/g" | sed "s/4.00000000/4/g" | sed "s/5.00000000/5/g" | sed "s/6.00000000/6/g" | sed "s/0.00000000/0/g" > dat.tmp
echo " dd11   dd12   dd21   dd22   dd31   dd32   dd41   dd42   dd51   dd52  tme             sm               ec              oc              pareto" > $1
cat dat.tmp >> $1
rm -rf dat.tmp
