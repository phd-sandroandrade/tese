// Install toolbox de testes de hipótese
// atomsInstall('casci') // O casci usa a funcao sort (que só existe no matlab), mudei para gsort

function pv=andersondarling(X)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=1
  error("incorrect number of arguments")
end
X=X(:)
n=length(X)
z=cdfnormal(-gsort(-X),mean(X),standev(X))
a=(-sum((1:2:(2*n-1))'.*(log(z)+log(1-z(n:-1:1))))/n-n)*(1+(0.75+2.25/n)/n)
pv=1-cdfjohnson(a,"B",3.9667391,1.4859832,0.1043856,3.7372864)
endfunction

function y=interpolate(minimumy, maximumy, minimumx, maximumx, alpha, x)
   if alpha == 0;
       y=minimumy+(maximumy-minimumy)*((x-minimumx)/(maximumx-minimumx));
   else
       y=minimumy+(maximumy-minimumy)*((%e^(alpha*((x-minimumx)/(maximumx-minimumx)))-1)/(%e^(alpha)-1));
   end
endfunction

clear x y z meanz maxz minz s p i j

p=300;
s=1;
x=0:s:p-1;
//target=4174.793;
target=423.0228;
//err=0.85;
err=0.95;

for j = 1:10
y(j,1)=0;
for i = 2:(p/s)
  y(j,i)=max(y(j,i-1),grand(1,1,"nor", interpolate(0, target*err, 1, 300, -2, i), interpolate(target/5, 1, 1, 300, -1, i)));
end
end

plot(x,mean(y,'r'),'b');
plot(x,max(y,'r'),'g');
plot(x,min(y,'r'),'k');
plot(x,stdev(y,'r'),'r');

printf("maximo ao final das iteracoes: %f",mean(y(:,300)));
printf("percentual do esperado: %f", 100*(mean(y(:,300))/target));
printf("iteracao onde se atinge 95%% do valor final: %d",min(find(mean(y,'r')>=0.95*mean(y(:,300)))));

for c1 = 2:300;
if andersondarling(y(:,c1)) >= 0.05 & levene(y(:,c1), 0.95*y(:,300)) >= 0.05 then
if tstnormalm2(y(:,c1),0.95*y(:,300),'>') < 0.05 then
    break
end
else
if wilcoxon2(y(:,c1),0.95*y(:,300)','>') < 0.05
    break
end
end
end

printf("Com a=0.05 chega-se a 95%% do valor final na iteração %d:",c1);

for c2 = 2:300;
if andersondarling(y(:,c2)) >= 0.05 & levene(y(:,c2), 0.95*y(:,300)) >= 0.05 then
if tstnormalm2(y(:,c2),0.95*y(:,300),'>') < 0.01 then
    break
end
else
if wilcoxon2(y(:,c2),0.95*y(:,300)','>') < 0.01
    break
end
end
end

printf("Com a=0.01 chega-se a 95%% do valor final na iteração %d:",c2);

for c = c1-1:c2+1;
p1 = andersondarling(y(:,c));
p2 = levene(y(:,c), 0.95*y(:,300));
if  p1 >= 0.05 & p2 >= 0.05 then
p3=tstnormalm2(y(:,c),0.95*y(:,300),'>');
printf("%d %f %f %f %s %f\n", c, mean(y(:,c)), p1, p2, 't-test', p3);
else
p3=wilcoxon2(y(:,c),0.95*y(:,300)','>');
printf("%d %f %f %f %s %f\n", c, mean(y(:,c)), p1, p2, 'wilcoxon', p3);
end
end

total=[x'+1,mean(y,'r')',max(y,'r')',min(y,'r')',stdev(y,'r')'];

write('teste.dat',total,"(5(f15.8,5x))");