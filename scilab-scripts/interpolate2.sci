function y=interpolate(minimumy, maximumy, minimumx, maximumx, alpha, x, invert)
   if alpha == 0;
       y=minimumy+(maximumy-minimumy)*((x-minimumx)/(maximumx-minimumx));
   else
       if invert == 1;
       y=minimumy+(maximumy-minimumy)*((1-%e^(alpha*((x-minimumx)/(maximumx-minimumx)))-1)/(%e^(alpha)-1));
       else
       y=minimumy+(maximumy-minimumy)*((%e^(alpha*((x-minimumx)/(maximumx-minimumx)))-1)/(%e^(alpha)-1));
       end
   end
endfunction

format(y, scientific = %f);

oldref = 25;
newref = 60;
maxtime = 108;
changetime = 48;

// Sistema massa-mola
t = 0:(3/(maxtime-changetime)):3;
m = 1.5;  // Mass : kg (quanto maior, maior o settling time)
b = 5; // Damper : Ns/m (quanto maior, maior o tempo de subida)
k = 45; // Spring : N/m (quanto maior, maior o overshoot)
System = k/(m*%s^2 + b*%s + k);
y1 = oldref + (newref-oldref)*csim('step', t , System);

mytime = cat(2, [0'], 5*changetime+5*t*(maxtime-changetime)/3);
throughput = cat(2, [oldref'], y1);

m = 2*m;  // Mass : kg (quanto maior, maior o settling time)
b = 2*b; // Damper : Ns/m (quanto maior, maior o tempo de subida)
k = 1.3333333*k; // Spring : N/m (quanto maior, maior o overshoot)
System = k/(m*%s^2 + b*%s + k);
y1 = oldref + (newref-oldref)*csim('step', t , System);
for i=2:cols(y1)
y1(1,i)=round(grand(1,1, 'nor', y1(1,i),2));
end
mthroughput = cat(2, [oldref'], y1);

for i=1:changetime
tmp(1,i)=abs(grand(1, 1, 'nor', interpolate(10, oldref,1,changetime,0,i), 1));
end
measured = cat(2, tmp, y1);

datum=zeros(cols(measured),31);
for i=1:cols(measured)
datum(i,:)=abs(grand(1,31,'nor',measured(1,i), interpolate(1,1,1,maxtime+1,0,i,1)));
end

plot(linspace(0,540,109),measured,'r');
plot(linspace(0,540,109),min(datum, 'c'),'y');
plot(linspace(0,540,109),max(datum, 'c'),'p');
plot(mytime,throughput, 'b');

write("throughput-model.dat", [mytime' throughput'], "(2(f15.8,5x))");
write("finaldatathroughput.dat", [linspace(0,540,109)' mean(datum, 'c') min(datum, 'c') max(datum, 'c')], "(4(f15.8,5x))");