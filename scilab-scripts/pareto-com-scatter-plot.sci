// Install plotmatrix
// atomsInstall('stixbox')

// Com 3 funções-objetivo
m=1000;
x1=grand(m,1,"def");
x2=grand(m,1,"def");
x3=grand(m,1,"def");
x=[x1,x2,x3];
y1=2*x1.*x2+x3;
y2=-3*x1+x2.^2-2*x3;
y3=sin(x1)-3*x2+3*x3;
y=[y1,y2,y3];
ylabels=["Y1","Y2","Y3"];
plotmatrix(y,y,"symbol","k.","ptsize",6);
[F_out,X_out,Ind_out] = pareto_filter(y,x)
plotmatrix(F_out,F_out,"symbol","b.","ptsize",6);

// Com 2 funções-objetivo
m=1000;
x1=grand(m,1,"def");
x2=grand(m,1,"def");
x=[x1,x2];
y1=2*x1.*x2+x1;
y2=-3*x1+x2.^2-2*x1;
y=[y1,y2];
ylabels=["Y1","Y2"];
plotmatrix(y,y,"symbol","k.","ptsize",6);
[F_out,X_out,Ind_out] = pareto_filter(y,x)
xset("mark size",5);
plotmatrix(F_out,F_out,"symbol","b.","ptsize",6);

// Para o SA:DuSE

function y=interpolate(minimumy, maximumy, minimumx, maximumx, alpha, x, invert)
   if alpha == 0;
       y=minimumy+(maximumy-minimumy)*((x-minimumx)/(maximumx-minimumx));
   else
       if invert == 1;
       y=minimumy+(maximumy-minimumy)*((1-%e^(alpha*((x-minimumx)/(maximumx-minimumx)))-1)/(%e^(alpha)-1));
       else
       y=minimumy+(maximumy-minimumy)*((%e^(alpha*((x-minimumx)/(maximumx-minimumx)))-1)/(%e^(alpha)-1));
       end
   end
endfunction

function [paretosize,tme,sm,ec,oc,f,F_out,X_out,Ind_out,i,total,m]=generate_pareto(p)
m=76; // Total de solucoes
force=-6;
minsettime=73;
maxsettime=221;
maxovershoot=50;

x1(1)=minsettime;
x1(p)=maxsettime;
for i=2:p-1
x1(i)=minsettime+(i-1)*(maxsettime-minsettime)/(p-1)+grand(1,1,'nor',0,0.5*(maxsettime-minsettime)/(p-1));
end
//x1=minsettime+(maxsettime-minsettime)*grand(p, 1, "bet", 2, 2);
x2=grand(m-p,1,"unf",minsettime, maxsettime);

y1=interpolate(0,maxovershoot,minsettime,maxsettime,force,x1,1);
y2=interpolate(0,maxovershoot,minsettime,maxsettime,force,x2,1);
for i=1:m-p
dominated=%f;
while ~dominated,
x2(i)=x2(i)+grand(1,1,"unf",0,max(0,maxsettime-x2(i)));
y2(i)=y2(i)+grand(1,1,"unf",0,max(0,maxovershoot-y2(i)));
for k=1:p,
if x2(i) > x1(k) & y2(i) > y1(k),
dominated=%t;
end;
end;
end;
end;
tme=cat(1,x1,x2);
sm=cat(1,y1,y2);
//scf(1);
//plot(x1,y1,'r.')
//plot(x2,y2,'b.')

// Efetividade e overhead devido ao grau de adaptacao do controle
x1(1)=0;
x1(p)=1;
for i=2:p-1
x1(i)=interpolate(0,1,0,4,0,grand(1,1,'uin',0,4),0);
end

ec1=interpolate(0,1,0,1,-2,x1,0);
oc1=interpolate(0,1,0,1, 4,x1,0);

for i=1:m-p
dominated=%f;
while ~dominated,
x1(i)=interpolate(0,1,0,4,0,grand(1,1,'uin',0,4),0);
x2(i)=interpolate(0,1,0,4,0,grand(1,1,'uin',0,4),0);
x3(i)=interpolate(0,1,0,4,0,grand(1,1,'uin',0,4),0);

//ec2(i)=(interpolate(0,1,0,1,-2,x1(i),0)+interpolate(0,1,0,1,-2,x2(i),0))/2;
//oc2(i)=(interpolate(0,1,0,1, 4,x1(i),0)+interpolate(0,1,0,1, 4,x2(i),0))/2;
ec2(i)=(interpolate(0,1,0,1,-2,x1(i),0)+interpolate(0,1,0,1,-2,x2(i),0)+interpolate(0,1,0,1,-2,x3(i),0))/3;
oc2(i)=(interpolate(0,1,0,1, 4,x1(i),0)+interpolate(0,1,0,1, 4,x2(i),0)+interpolate(0,1,0,1,-2,x3(i),0))/3;
for k=1:p,
if ec2(i) < ec1(k) & oc2(i) > oc1(k),
dominated=%t;
end;
end;
end;
end;

clear x1;
// Efetividade e overhead devido à forma de cooperação
x1(1)=0;
x1(p)=1;
for i=2:p-1
x1(i)=interpolate(0,1,0,5,0,grand(1,1,'uin',0,5),0);
end

ec3=interpolate(0,1,0,1,-2,x1,0);
oc3=interpolate(0,1,0,1, 4,x1,0);

for i=1:m-p
dominated=%f;
while ~dominated,
x1(i)=interpolate(0,1,0,5,0,grand(1,1,'uin',0,5),0);
x2(i)=interpolate(0,1,0,5,0,grand(1,1,'uin',0,5),0);
x3(i)=interpolate(0,1,0,5,0,grand(1,1,'uin',0,5),0);

//ec2(i)=(interpolate(0,1,0,1,-2,x1(i),0)+interpolate(0,1,0,1,-2,x2(i),0))/2;
//oc2(i)=(interpolate(0,1,0,1, 4,x1(i),0)+interpolate(0,1,0,1, 4,x2(i),0))/2;
ec4(i)=(interpolate(0,1,0,1,-2,x1(i),0)+interpolate(0,1,0,1,-2,x2(i),0)+interpolate(0,1,0,1,-2,x3(i),0))/3;
oc4(i)=(interpolate(0,1,0,1, 4,x1(i),0)+interpolate(0,1,0,1, 4,x2(i),0)+interpolate(0,1,0,1,-2,x3(i),0))/3;
for k=1:p,
if ec2(i) < ec1(k) & oc2(i) > oc1(k),
dominated=%t;
end;
end;
end;
end;

betapar=0.25;

//ec=cat(1,ec1,ec2);
//oc=cat(1,oc1,oc2);
ec=cat(1,ec1*betapar+ec3*(1-betapar),ec2*betapar+ec4*(1-betapar));
oc=cat(1,oc1*betapar+oc3*(1-betapar),oc2*betapar+oc4*(1-betapar));

//scf(2);
//plot(x1,y1,'r.')
//plot(x2,y2,'b.')

f=[tme,sm,-ec,oc];
// 1-SISO/MIMO          2-SISO/MIMO          3-LEI DE CONTROLE    4-LEI DE CONTROLE    5-SINTONIA           6-SINTONIA           7-ADAPTACAO          8-ADAPTACAO          9-COOPERAÇÃO         10-COOPERAÇÃO
i=[grand(m,1,"uin",0,1),grand(m,1,"uin",0,1),grand(m,1,"uin",0,6),grand(m,1,"uin",0,6),grand(m,1,"uin",0,6),grand(m,1,"uin",0,6),grand(m,1,"uin",0,4),grand(m,1,"uin",0,4),grand(m,1,"uin",0,5),grand(m,1,"uin",0,5)];
[B,ind]=gsort(sm,'r','i');
i(ind(1),5)=0;
i(ind(2),5)=1;
for e=3:m-2
i(ind(e),5)=grand(1,1,"uin",0,6);
end
i(ind(m-1),5)=3;
i(ind(m),5)=2;

[B,ind]=gsort(ec,'r','i');
i(ind(1),7)=0;
i(ind(2),7)=0;
for e=3:m-2
i(ind(e),7)=floor(e/(m/5));
end
i(ind(m-1),7)=4;
i(ind(m),7)=4;

i(ind(1),9)=0;
i(ind(2),9)=0;
for e=3:m-2
i(ind(e),9)=grand(1,1,"uin",0,5);
end
i(ind(m-1),9)=5;
i(ind(m),9)=5;

[F_out,X_out,Ind_out] = pareto_filter(f,i);
F_out(:,3)=-F_out(:,3);
f(:,3)=-f(:,3);
total=[i,f,[0:m-1]'];
total(:,15)=0;
total(Ind_out,15)=1;

paretosize=size(F_out);
endfunction

ylabels=["TME","SM","EC", "OC"];
xlabels=["TME","SM","EC", "OC"];
scf(3);
plotmatrix(f,"symbol","k.","ptsize",6, "xlabels", xlabels, "ylabels", ylabels);
scf(4);
plotmatrix(f,"symbol","k.","ptsize",6, "xlabels", xlabels, "ylabels", ylabels);
plotmatrix(F_out,F_out,"symbol","b.","ptsize",6, "xlabels", xlabels, "ylabels", ylabels);

write('pop-teste.dat',total,"(15(f15.8,5x))");
write('dominated-teste.dat',total(find(total(:,15)==0),:),"(15(f15.8,5x))");
write('pareto-teste.dat',total(find(total(:,15)==1),:),"(15(f15.8,5x))");

f=[tme,sm];
[F_out,X_out,Ind_out] = pareto_filter(f,i);
total=total(Ind_out,:);
[s,idx]=gsort(F_out(:,1),'r','i');
total=total(idx,:);
write('pareto-tme-sm-teste.dat',total,"(15(f15.8,5x))");

f=[tme,sm,ec,oc];
total=[i,f,[0:m-1]'];

f=[-ec,tme];
[F_out,X_out,Ind_out] = pareto_filter(f,i);
F_out(:,1)=-F_out(:,1);
f(:,1)=-f(:,1);
total=total(Ind_out,:);
[s,idx]=gsort(F_out(:,1),'r','i');
total=total(idx,:);
write('pareto-ec-tme-teste.dat',total,"(15(f15.8,5x))");

f=[tme,sm,ec,oc];
total=[i,f,[0:m-1]'];

f=[-ec,sm];
[F_out,X_out,Ind_out] = pareto_filter(f,i);
F_out(:,1)=-F_out(:,1);
f(:,1)=-f(:,1);
total=total(Ind_out,:);
[s,idx]=gsort(F_out(:,1),'r','i');
total=total(idx,:);
write('pareto-ec-sm-teste.dat',total,"(15(f15.8,5x))");

f=[tme,sm,ec,oc];
total=[i,f,[0:m-1]'];

f=[tme,oc];
[F_out,X_out,Ind_out] = pareto_filter(f,i);
total=total(Ind_out,:);
[s,idx]=gsort(F_out(:,1),'r','i');
total=total(idx,:);
write('pareto-tme-oc-teste.dat',total,"(15(f15.8,5x))");

f=[tme,sm,ec,oc];
total=[i,f,[0:m-1]'];

f=[sm,oc];
[F_out,X_out,Ind_out] = pareto_filter(f,i);
total=total(Ind_out,:);
[s,idx]=gsort(F_out(:,1),'r','i');
total=total(idx,:);
write('pareto-sm-oc-teste.dat',total,"(15(f15.8,5x))");

f=[tme,sm,ec,oc];
total=[i,f,[0:m-1]'];

f=[-ec,oc];
[F_out,X_out,Ind_out] = pareto_filter(f,i);
F_out(:,1)=-F_out(:,1);
f(:,1)=-f(:,1);
total=total(Ind_out,:);
[s,idx]=gsort(F_out(:,1),'r','i');
total=total(idx,:);
write('pareto-ec-oc-teste.dat',total,"(15(f15.8,5x))");