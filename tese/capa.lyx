#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass scrbook
\begin_preamble
% increases link area for cross-references and autoname them
% if you change the document language to e.g. French
% you must change "extrasenglish" to "extrasfrench"
\AtBeginDocument{%
 \renewcommand{\ref}[1]{\mbox{\autoref{#1}}}
}
\def\refnamechanges{%
 \renewcommand*{\equationautorefname}[1]{}
 \renewcommand{\sectionautorefname}{sec.\negthinspace}
 \renewcommand{\subsectionautorefname}{sec.\negthinspace}
 \renewcommand{\subsubsectionautorefname}{sec.\negthinspace}
 \renewcommand{\figureautorefname}{Fig.\negthinspace}
 \renewcommand{\tableautorefname}{Tab.\negthinspace}
}
\@ifpackageloaded{babel}{\addto\extrasportuguese{\refnamechanges}}{\refnamechanges}

% in case somebody want to have the label "Equation"
%\renewcommand{\eqref}[1]{Equation~(\negthinspace\autoref{#1})}

% that links to image floats jumps to the beginning
% of the float and not to its caption
\usepackage[figure]{hypcap}

% the pages of the TOC is numbered roman
% and a pdf-bookmark for the TOC is added
\let\myTOC\tableofcontents
\renewcommand\tableofcontents{%
  \frontmatter
  \pdfbookmark[1]{\contentsname}{}
  \myTOC
  \mainmatter }

% makes caption labels bold
% for more info about these settings, see
% http://mirrors.ctan.org/macros/latex/contrib/koma-script/doc/scrguien.pdf
\setkomafont{captionlabel}{\bfseries}
\setcapindent{1em}

% enables calculations
\usepackage{calc}

% fancy page header/footer settings
% for more information see section 9 of
% ftp://www.ctan.org/pub/tex-archive/macros/latex2e/contrib/fancyhdr/fancyhdr.pdf
\renewcommand{\chaptermark}[1]{\markboth{#1}{#1}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}

% increases the bottom float placement fraction
\renewcommand{\bottomfraction}{0.5}

% avoids that floats are placed above its sections
\let\mySection\section\renewcommand{\section}{\suppressfloats[t]\mySection}
\usepackage{setspace}
\end_preamble
\use_default_options true
\begin_modules
customHeadersFooters
\end_modules
\maintain_unincluded_children false
\language brazilian
\language_package default
\inputencoding auto
\fontencoding global
\font_roman lmodern
\font_sans lmss
\font_typewriter lmtt
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref true
\pdf_bookmarks true
\pdf_bookmarksnumbered true
\pdf_bookmarksopen true
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle true
\pdf_quoted_options "pdfpagelayout=OneColumn, pdfnewwindow=true, pdfstartview=XYZ, plainpages=false"
\papersize a4paper
\use_geometry true
\use_package amsmath 2
\use_package amssymb 2
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date true
\justification true
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 3cm
\topmargin 1.3cm
\rightmargin 2cm
\bottommargin 2.5cm
\headheight 12pt
\headsep 15pt
\footskip 28pt
\secnumdepth 3
\tocdepth 3
\paragraph_separation skip
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 2
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Subject

\size small
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
pagenumbering{gobble}
\end_layout

\end_inset


\size default

\begin_inset Graphics
	filename logos.png
	lyxscale 50
	scale 50

\end_inset


\begin_inset Newline newline
\end_inset


\size footnotesize

\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
setlength{
\backslash
parskip}{-0.1in}
\end_layout

\end_inset

Universidade Federal da Bahia
\begin_inset Newline newline
\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
setlength{
\backslash
parskip}{-0.1in}
\end_layout

\end_inset

Universidade Salvador
\begin_inset Newline newline
\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
setlength{
\backslash
parskip}{-0.1in}
\end_layout

\end_inset

Universidade Estadual de Feira de Santana
\size default

\begin_inset VSpace 57pt
\end_inset


\end_layout

\begin_layout Title
Projeto Arquitetural Automatizado
\begin_inset Newline newline
\end_inset

de Sistemas Self-Adaptive
\end_layout

\begin_layout Subtitle
Uma Abordagem Baseada em Busca
\begin_inset Newline newline
\end_inset


\begin_inset VSpace 3cm
\end_inset


\end_layout

\begin_layout Author

\size normal
por
\series bold

\begin_inset Newline newline
\end_inset

Sandro Santos Andrade
\end_layout

\begin_layout Publishers

\series bold
\size normal
Tese de Doutorado
\series default
\size default

\begin_inset Newline newline
\end_inset


\size small

\begin_inset VSpace vfill
\end_inset

SALVADOR
\begin_inset Newline newline
\end_inset

Dezembro/2014
\begin_inset Newpage clearpage
\end_inset


\end_layout

\end_body
\end_document
