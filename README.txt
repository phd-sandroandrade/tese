Como usar o device de exportacao para o Tikz de dentro do R:

1) Instalar os pacotes GGally e tikzDevice no R. No console do R, digitar:

install.packages("GGally")
install.packages( 'tikzDevice' )

2) Para exportar um gráfico do R para o tikz, executar no console do R:

tikz("caminho para o arquivo .tikz") // Para gerar pdf, use pdf("caminho para o arquivo .pdf")
<executar o comando do R para geração do gráfico>
dev.off()

3) Mover o arquivo .tikz gerado para a mesma pasta do arquivo .tex/.lyx

4) Incluir o arquivo .tikz no .tex/.lyx usando \input{<arquivo .tikz>}

5) O preâmbulo do documento latex deve conter as linhas:

\usepgfplotslibrary{external} 
\tikzexternalize

6) O comando pdflatex deve ser executado com a opção -shell-escape. No lyx
isso pode ser configurado no menu Tools -> Preferences -> Converters. Na
opção "LaTeX (pdflatex) -> PDF (pdflatex)" mudar o parâmetro "Converter:"
para "pdflatex -shell-escape $$i"

7) Pode-se mudar o tamanho do grafico tikz gerado com o comando:

\resizebox{1\textwidth}{!}{
\input{<arquivo .tikz>}
}

8) Executando scripts R da linha de comando:

R CMD BATCH <script .r>

Links sobre scatterplot matrices com R:

http://stackoverflow.com/questions/21810675/ggallyggpairs-plot-without-gridlines-when-plotting-correlation-coefficient
http://tgmstat.wordpress.com/2013/11/13/plot-matrix-with-the-r-package-ggally/
http://gastonsanchez.wordpress.com/2012/08/27/scatterplot-matrices-with-ggplot/
https://biostatmatt.com/archives/2398
http://www.r-bloggers.com/example-9-17-much-better-pairs-plots/
http://cran.r-project.org/web/packages/GGally/index.html
http://cran.r-project.org/web/packages/gpairs/index.html
http://www.r-bloggers.com/scatterplot-matrices-in-r/
http://www.r-bloggers.com/scatterplot-matrices/
http://www.statmethods.net/graphs/scatterplot.html
http://gettinggeneticsdone.blogspot.com.br/2011/07/scatterplot-matrices-in-r.html

Scatterplot com histogramas (ou qualquer outro gráfico) marginais:
http://tex.stackexchange.com/questions/193059/scatterplot-with-marginal-histograms-using-pgfplots

Outras ferramentas para plotagens:

http://www.adeptscience.co.uk/products/data-analysis-software/origin
http://www.originlab.com/
http://root.cern.ch/drupal/
